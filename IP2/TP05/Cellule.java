public class Cellule {
    private int valeur;
    private Cellule suivant;

    public Cellule(int x) {
        this.valeur = x;
    }

    public Cellule(int x, Cellule suivant) {
        this(x);
        this.suivant = suivant;
    }

    public int getValeur() {
        return valeur;
    }

    public Cellule getSuivant() {
        return suivant;
    }

    public String description() {
        return (valeur + ((suivant != null)? ";" + suivant.description() : ""));
    }

    public int taille() {
        return 1 + ((this.suivant != null) ? suivant.taille() : 0);
    }

    public int somme() {
        return this.valeur + ((this.suivant != null) ? this.suivant.somme() : 0);
    }

    public boolean egal(Cellule arg) {
        return (arg != null &&
                (this.valeur == arg.valeur &&
                        (this.suivant == null && arg.suivant == null || this.suivant.egal(arg.suivant)
                        )
                )
        );
    }

    public void ajouter_en_i(int i, int v) {
        if (i == 1 || this.suivant == null) {
            this.suivant = new Cellule(v, this.suivant);
        } else {
            this.suivant.ajouter_en_i(i-1, v);
        }
    }

    public boolean supprimer_en_i(int i) {
        if (i == 1) {
            this.suivant = this.suivant.suivant;
            return true;
        } else if (this.suivant != null) {
            return this.suivant.supprimer_en_i(i-1);
        }
        return false;
    }

    public int supprimer_k_premieres_occ(int k, int v) {
        if (this.suivant != null && k > 0) {
            if (this.suivant.valeur == v) {
                this.suivant = this.suivant.suivant;
                return 1 + this.supprimer_k_premieres_occ(k - 2, v);
            } else {
                return this.suivant.supprimer_k_premieres_occ(k, v);
            }
        }
        return 0;
    }

   public void obtenir_sous_liste_inf_k(int k, ListeDEntiers acc) {
        if (this.valeur < k) {
            acc.add(this.valeur);
        }
        if (this.suivant != null) {
            this.suivant.obtenir_sous_liste_inf_k(k, acc);
        }
   }

   public void reverseAux(ListeDEntiers lst) {
        lst.add(this.valeur);
        if (this.suivant != null) {
            this.suivant.reverseAux(lst);
        }
   }

   public void detacher() {
        this.suivant = null;
   }
}
