public class Main {
    public static void main(String[] args) {
        ListeDEntiers lst = new ListeDEntiers();
        System.out.println(lst.description());
        lst.add(7737);
        System.out.println(lst.taille());
        System.out.println(lst.description());
        lst.add(0);
        lst.add(5);
        System.out.println(lst.description());
        System.out.println(lst.taille());
        System.out.println(lst.somme());

        ListeDEntiers lst2 = new ListeDEntiers();
        lst2.add(7737); lst2.add(0);
        System.out.println(lst.egal(lst2));
        lst2.add(5);
        System.out.println(lst.egal(lst2));

        lst.ajouter_en_i(-1, -10);
        lst.ajouter_en_i(3, 30);
        lst.ajouter_en_i(6, 50);
        System.out.println(lst.description());

        lst.supprimer_en_i(0);
        System.out.println(lst.description());
        lst.supprimer_en_i(3);
        System.out.println(lst.supprimer_en_i(5));
        System.out.println(lst.description());


        lst.add(3);
        lst.add(3);
        lst.add(4);
        lst.add(3);
        System.out.println(lst.description());
        lst.supprimer_k_premieres_occ(5, 3);
        System.out.println(lst.description());
        lst.add(20);
        ListeDEntiers slst = lst.obtenir_sous_liste_inf_k(20);
        System.out.println(slst.description());
    }
}