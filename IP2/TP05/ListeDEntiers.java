public class ListeDEntiers {
    private Cellule premier;

    public ListeDEntiers(Cellule premier) {
        this.premier = premier;
    }

    public ListeDEntiers() {
        this(null);
    }

    public void add(int x) {
        this.premier = new Cellule(x, this.premier);
    }

    public String description() {
        String result = "[";
        if (premier != null) {
            result += premier.description();
        }
        result += "]";
        return result;
    }

    public int taille() {
        return (this.premier != null) ? premier.taille() : 0;
    }

    public int somme() {
        return (this.premier != null) ? premier.somme() : 0;
    }

    public boolean egal(ListeDEntiers arg) {
        if (this.premier == null) {
            return arg.premier == null;
        }
        return this.premier.egal(arg.premier);
    }

    public void ajouter_en_i(int i, int v) {
        if (i <= 0) {
            this.add(v);
        } else if (this.premier != null) {
            this.premier.ajouter_en_i(i, v);
        }
    }

    public boolean supprimer_en_i(int i) {
        if (i < 0) return false;
        else if (i == 0) {
            this.premier = this.premier.getSuivant();
        } else {
            return this.premier.supprimer_en_i(i);
        }
        return true;
    }

    public int supprimer_k_premieres_occ(int k, int v) {
        if (k > 0 && this.premier != null) {
            if (this.premier.getValeur() == v) {
                int c = this.premier.supprimer_k_premieres_occ(k - 1, v);
                this.premier = this.premier.getSuivant();
                return c + 1;
            } else {
                return this.premier.supprimer_k_premieres_occ(k, v);
            }
        }
        return 0;
    }

    public ListeDEntiers obtenir_sous_liste_inf_k(int k) {
        ListeDEntiers acc = new ListeDEntiers();
        if (this.premier != null) {
            if (this.premier.getValeur() < k) {
                acc.add(this.premier.getValeur());
            }
            this.premier.obtenir_sous_liste_inf_k(k, acc);
        }
        acc.reverse();
        return acc;
    }

    public void reverse() {
        if (this.premier != null && this.premier.getSuivant() != null) {
            Cellule p = this.premier;
            this.premier.getSuivant().reverseAux(this);
            p.detacher();
        }
    }
}
