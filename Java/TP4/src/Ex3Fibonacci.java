import java.util.Arrays;

public class Ex3Fibonacci {
    public static int[] fibonacci(int n) {
        int[] result = new int[n];
        result[0] = 0;
        result[1] = 1;
        for (int i = 2; i < n; i++) {
            result[i] = result[i - 2] + result[i - 1];
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(fibonacci(10)));
    }
}
