import java.util.Arrays;

public class Ex6Tri {
    /*Prend en argument un tableau d’entiers tab, qu’on considère déjà trié et un entier x. Renvoie la position
      dans tab dans laquelle on devrait insérer x, pour que le tableau obtenu reste trié*/
    public static int position(int[] tab, int x) {
        if (x < tab[0]) return 0;
        for (int i = 1; i < tab.length; i++) {
            if (x >= tab[i - 1] && x <= tab[i]) {
                return i;
            }
        }
        if (x > tab[tab.length - 1]) return tab.length;
        return -1;
    }

    // Renvoie un tableau contenant les mêmes éléments que tab, en insérant x à la position pos
    public static int[] insert(int[] tab, int pos, int x) {
        if (pos > tab.length) {
            return tab;
        }
        int[] result = new int[tab.length + 1];
        boolean added = false;
        for (int i = 0; i < result.length; i++) {
            if (i == pos) {
                result[i] = x;
                added = true;
            } else {
                result[i] = tab[(added) ? (i - 1) : i];
            }
        }
        return result;
    }


    // Prend un tableau tab et le renvoie trié
    public static int[] sort(int[] tab) {
        if (tab.length == 0) return tab;
        int[] result = new int[1];
        result[0] = tab[0];
        for (int i = 1; i < tab.length; i++) {
            result = insert(result, position(result, tab[i]), tab[i]);
        }
        return result;
    }

    public static void main(String[] args) {
//        int[] tab={0,2,4,6,7,8};
//        System.out.println(position(tab,1));
//        System.out.println(position(tab,-5));
//        System.out.println(position(tab,10));
//
//        int[] tab2={2,5,4,3};
//        System.out.println(Arrays.toString(insert(tab2, 0, 1)));
//        System.out.println(Arrays.toString(insert(tab2, 2, 100)));
        int[] tab = {40, 1, 20, 3, 8, 6};
        System.out.println(Arrays.toString(sort(tab)));
        int[] tab2 = {};
        System.out.println(Arrays.toString(sort(tab2)));
    }
}
