import java.util.Random;

public class RandomArray {
    static Random rand = new Random();

    /* ************************************************************************** */


    //  QUESTION 1
    // Renvoie un tableau de taille n dont les cases sont remplies par des entiers aléatoires compris entre 0 et (n-1)
    public static int[] createRandomArray(int n) {
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            result[i] = rand.nextInt(n);
        }
        return result;
    }


    // QUESTION 2
    // Renvoie un tableau de taille 3 contenant dans sa première case le minimum, dans sa deuxième le maximum et dans sa
    // troisième la partie entière de la moyenne des éléments de a
    public static int[] minMaxAverage(int[] a) {
        if (a.length == 0) return new int[]{0,0,0};
        int total = 0;
        int max = a[0];
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            total += a[i];
            if (a[i] > max) {
                max = a[i];
            }
            if (a[i] < min) {
                min = a[i];
            }
        }
        int avg = total / a.length;
        int[] result = {min, max, avg};
        return result;
    }


    // QUESTION 3
    // Renvoie un tableau contenant, à la case d’in-dice i, le nombre d’occurrences de i dans a.
    public static int[] occurrences(int[] a) {
        int[] results = new int[1 + minMaxAverage(a)[1]];
        for (int i = 0; i < a.length; i++) {
            results[a[i]]++;
        }
        return results;
    }

    // QUESTION 4a
    // Renvoie un tableau trié contenant les mêmes éléments que a
    public static int[] countingSort(int[] a) {
        int[] nbOccurences = occurrences(a);
        int[] result = new int[a.length];
        int indexResult = 0;
        for (int i=0; i < nbOccurences.length; i++) {
            for (int j = 0; j < nbOccurences[i]; j++) {
                result[indexResult] = i;
                indexResult++;
            }
        }
        return result;
    }

    // QUESTION 4b
    // Même procédé que countingSort, mais le contenu de a est directement modifié après l'appel de la fonction
    public static void countingSort2(int[] a) {
        int[] nbOccurences = occurrences(a);
        int indexResult = 0;
        for (int i=0; i < nbOccurences.length; i++) {
            for (int j = 0; j < nbOccurences[i]; j++) {
                a[indexResult] = i;
                indexResult++;
            }
        }
    }


    /******************************************/
    /*     Fonctions auxiliaires              */

    /******************************************/

    public static boolean intArrayEquals(int[] a, int[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public static void printIntArray(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }

    /* ********************************************************** */
    /*      Fonction Principale                                   */
    /* ********************************************************** */


    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Entrez un entier positif (la taille du tableau):");
//        int n = sc.nextInt();
//        sc.nextLine();
//        int[] a = createRandomArray(n);
//        printIntArray(a);
          printIntArray(minMaxAverage(createRandomArray(100)));
//        int[] a = {1, 3, 0, 0, 0, 1};
//        printIntArray(occurrences(a));
//        printIntArray(countingSort(a));
        int[] a = createRandomArray(100);
        int[] b = countingSort(a);
        countingSort2(a);
        System.out.println(intArrayEquals(a,b));
    }
}
