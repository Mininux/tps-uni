import java.util.Arrays;

public class Ex2Shift {
    // Renvoie une copie d'un tableau d'entiers où toutes les valeurs sont décalées d'une case vers la droite
    public static int[] shift1(int[] tab) {
        if (tab.length == 0) return tab;

        int[] result = new int[tab.length];
        for (int i = 1; i < tab.length; i++) {
            result[i] = tab[i - 1];
        }
        // Ou bien
        // System.arraycopy(tab, 1, result, 0, tab.length - 1);

        // On ajoute le dernier élément au tout début :
        result[0] = tab[tab.length - 1];
        return result;
    }


    /* En java, le modulo ne correspond pas précisemment au reste de la division euclidienne, en effet il peut être
       négatif. Pour la suite, on aura besoin d'une version qui renvoie toujours le reste *positif* (sinon on aurait
       une erreur out of bounds). */
    public static int positiveModulo(int n, int k) {
        int m = n % k;
        if (m < 0) m += k;
        return m;
    }


    /* Renvoie une copie d'un tableau d'entiers où toutes les valeurs sont décalées de n case vers la droite si n est
      positif, de -n cases vers la gauche si n est négatif. */
    public static int[] shift(int[] tab, int n) {
        if (tab.length == 0) return tab;

        int[] result = new int[tab.length];
        for (int i = 0; i < tab.length; i++) {
            // On utilise le modulo positif pour revenir au début du tableau si nécessaire et éviter un out of bounds.
            // Par ailleurs, cette implémentation permet de traiter directement les cas n < 0
            result[i] = tab[positiveModulo(i - n, tab.length)];
        }

        return result;
    }

    public static void main(String[] args) {
        int t[] = {1000, 1, 2, 3};
        System.out.println(Arrays.toString(shift1(t)));
        System.out.println(Arrays.toString(shift(t, 1)));
        System.out.println(Arrays.toString(shift(t, 2)));
        System.out.println(Arrays.toString(shift(t, 5)));
        System.out.println(Arrays.toString(shift(t, -1)));
        System.out.println(Arrays.toString(shift(t, -3)));
        System.out.println(Arrays.toString(shift(t, -5)));
    }
}
