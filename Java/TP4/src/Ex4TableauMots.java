import java.util.Arrays;

public class Ex4TableauMots {
    // Affiche le mot obtenu en concaténant les caractères contenus dans tab.
    public static void letters2word(char[] tab) {
        // Contiendra la chaîne de caractère finale
        String result = "";
        // On parcourt le tableau...
        for (int i = 0; i < tab.length; i++) {
            result += tab[i];
        }
        // Ou bien
        // String result = new String(tab);

        System.out.println(result);
    }

    /* Affiche le mot obtenu en concaténant les lettres du tableau tab1, mais la lettre sur la position i dans tab1 est
     *  répétée autant de fois que l’indique le numéro sur la position i dans tab2.
     */
    public static void stutterword(char[] tab1, int[] tab2) {
        if (tab1.length != tab2.length) System.out.println("Erreur");
        else {
            String result = "";
            for (int i = 0; i < tab1.length; i++) {
                for (int j = 0; j < tab2[i]; j++) {
                    result += tab1[i];
                }
            }
            System.out.println(result);
        }
    }

    // Renvoie le tableau des lettres d'une chaîne de caractères.
    public static char[] word2letters(String s) {
        char[] result = new char[s.length()];
        for (int i = 0; i < result.length; i++) {
            result[i] = s.charAt(i);
        }
        return result;
    }

    // Renvoie si une lettre est contenue dans un tableau de caractères
    public static boolean search(char[] tab, int x) {
        // Question 1:
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == x) {
                return true;
            }
        }
        return false;
    }

    // Renvoie le tableau des lettres d'une chaîne de caractères, sans doublons

    // Première implémentation, lente car nécessite d'appeler plusieurs fois search, mais fonctionne sur tous les
    // caractères existants.
    public static char[] letters(String s) {
        // On crée un tableau auxiliaire dont la taille sera forcément suffisante
        char[] aux = new char[s.length()];
        /* Contiendra la taille finale qu'aura le tableau qu'on renvoie et permettra de savoir où placer les prochaines
        * lettres */
        int finalSize = 0;
        // On parcourt le tableau...
        for (int i = 0; i < aux.length; i++) {
            // Si la lettre à la position actuelle n'a pas déjà été traitée...
            if (!search(aux, s.charAt(i))) {
                aux[finalSize] = s.charAt(i);
                finalSize++;
            }
        }

        char[] result = new char[finalSize];
        for (int i = 0; i < finalSize; i++) {
            result[i] = aux[i];
        }
        // Ou bien:
        // System.arraycopy(aux, 0, result, 0, finalSize);
        return result;
    }


    // 2e implémentation, celle-ci est plus rapide, mais ne fonctionne que pour des caractères alphabétiques.
    public static char[] letters2(String s) {
        char[] aux = new char[s.length()];
        int finalSize = 0;
        boolean[] charDone = new boolean[58];
        for (int i =0; i < aux.length; i++) {
            if (!charDone[s.charAt(i) - 'A']) {
                aux[finalSize] = s.charAt(i);
                charDone[s.charAt(i) - 'A'] = true;
                finalSize++;
            }
        }

        char[] result = new char[finalSize];
        for (int i = 0; i < finalSize; i++) {
            result[i] = aux[i];
        }
        // Ou bien:
        // System.arraycopy(aux, 0, result, 0, finalSize);
        return result;
    }

    public static void main(String[] args) {
        char[] tab = {'p', 'l', 'a', 'c', 'a', 'r', 'd'};
        letters2word(tab);

        char[] tab1 = {'a', 'b', 'c', 'd'};
        int[] tab2 = {2, 2, 3, 4};
        stutterword(tab1, tab2);
        System.out.println(Arrays.toString(word2letters("placard")));
        System.out.println(Arrays.toString(letters2("electroacoustique")));

        System.out.println(Arrays.toString(letters2("ElectroacousTiqueZz")));
    }
}
