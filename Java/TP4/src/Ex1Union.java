import java.util.Arrays;

public class Ex1Union {
    // Question 1:
    /* Renvoie true si x appartient au tableau tab */
    public static boolean search(int[] tab, int x) {
        for (int i = 0; i < tab.length; i++) { // On parcourt le tableau
            if (tab[i] == x) {   // Si la valeur à la position i du tableau est celle que l'on cherche...
                return true;
            }
        }
        return false;
    }

    // Question 2:
    // Renvoie l'union de deux tableaux d'entiers
    public static int[] union(int[] tab1, int[] tab2) {
        // On crée un tableau auxiliaire dont taille est forcément suffisante pour contenir l'union de tab1 et tab2
        int[] aux = new int[tab1.length + tab2.length];
        /* Contiendra la taille finale qu'aura le tableau qu'on renvoie et permettra de savoir où placer les éléments
           du tableau2. On l'initialise à la taille du tableau1 car les nombres du tableau2 seront insérés après la fin
           des éléments du tableau1. */
        int finalSize = tab1.length;
        // On remplit le début du tableau aux avec les nombres du tableau1
        for (int i = 0; i < tab1.length; i++) {
            aux[i] = tab1[i];
        }
        // Ou bien
        // System.arraycopy(tab1, 0, aux, 0, tab1.length);

        // Pour chaque élément du tableau1, s'il n'est présent déjà présent dans le tableau1, alors on l'ajoute.
        for (int i = 0; i < tab2.length; i++) {
            if (!search(tab1, tab2[i])) {
                aux[finalSize] = tab2[i];
                // On augmente finalSize pour savoir où insérer les prochains nombres
                finalSize++;
            }
        }
        int[] result = new int[finalSize];
        // le tableau aux est trop grand, on crée un autre tableau contenant uniquement ce dont on a besoin
        for (int i = 0; i < finalSize; i++) {
            result[i] = aux[i];
        }
        // Ou bien
        // System.arraycopy(aux, 0, result, 0, finalSize);
        return result;
    }

    public static void main(String[] args) {
        // Test question 1:
        int[] tab = {6, 20, 12, 1000, 8};
        System.out.println(search(tab, 12));
        System.out.println(!search(tab, 50));

        // Test question 2:
        int[] tab1 = {6, 20, 12, 1000, 8};
        int[] tab2 = {2, 8, 6, 7, 12};
        int[] tab1Utab2 = {6, 20, 12, 1000, 8, 2, 7};
        System.out.println(Arrays.equals(union(tab1, tab2), tab1Utab2));
    }
}