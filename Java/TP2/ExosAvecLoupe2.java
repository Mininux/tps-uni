class Cadre {
    // Question 1:
    public static String line(int n) {
        return "#".repeat(n);
    }


    public static String line_bis(int n) {
        return "-".repeat(n);
    }

    // Question 2:
    /*public static void frame(String text) {
        int frameWidth = text.length() + 2;
        System.out.println("+" + line_bis(frameWidth) + "+");
        System.out.println("| " + text + " |");
        System.out.println("+" + line_bis(frameWidth) + "+");
    }*/

    // Question 3:
    public static void frame(String text) {
        String[] lines = text.split("\n");
        int maxWidth = 0;  // Contiendra la longueur de la plus longue ligne, qui sera la taille du cadre
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].length() >= maxWidth) {
                maxWidth = lines[i].length();
            }
        }
        String frameLine = "+" + line_bis(maxWidth + 2) + "+"; // La ligne du haut du cadre et du bas du cadre

        System.out.println(frameLine);
        for (int i = 0; i < lines.length; i++) {
            String currentLine = lines[i];
            currentLine += " ".repeat(maxWidth - currentLine.length()); // On ajoute les espaces manquants pour les lignes trop courtes
            currentLine = "| " + currentLine + " |";
            System.out.println(currentLine);
        }
        System.out.println(frameLine);
    }


    public static void frame(String text) {
        String[] lignes = text.split("\n");
        int maxWidth = 0;  // Contiendra la longueur de la plus longue ligne, qui sera la taille du cadre
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].length() >= maxWidth) {
                maxWidth = lines[i].length();
            }
        }
        String frameLine = "+" + line_bis(maxWidth + 2) + "+"; // La ligne du haut du cadre et du bas du cadre

        System.out.println(frameLine);
        for (int i = 0; i < lines.length; i++) {
            String currentLine = lines[i];
            currentLine += " ".repeat(maxWidth - currentLine.length()); // On ajoute les espaces manquants pour les lignes trop courtes
            currentLine = "| " + currentLine + " |";
            System.out.println(currentLine);
        }
        System.out.println(frameLine);
    }


    public static void main(String[] args) {
        // tests:
        frame("Hello World!");
        frame("Ligne1\nLigne numéro deux\nLigne no 3");
    }
}


class Palindromes {
    // Question 1:
    public static String reverse(String text) {
        String reversed = "";
        for (int i = text.length() - 1; i >= 0; i--) { // Pour chaque lettre, on prend la lettre opposée
            reversed += text.charAt(i);
        }
        return reversed;
    }

    // Question 2:
    public static boolean palindrome(String text) {
        return reverse(text).equals(text);
    }

    // Question 3:
    public static boolean palindrome_bis(String text) {
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)) { // On vérifie que pour chaque lettre, celle à l'opposée est la meme
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        // tests
        System.out.println(reverse("hello").equals("olleh"));
        System.out.println(palindrome_bis("rotor") == true);
        System.out.println(palindrome_bis("ressasser") == true);
        System.out.println(palindrome_bis("abricot") == false);
    }
}


class NombresAmicaux {
    // Question 1:
    public static int sumDiv(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) { // Pour chaque entier inférieur à n, on teste si il divise n.
            if (n % i == 0) { // Si i divise n
                sum += i;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        // Test question 1:
        System.out.println(sumDiv(6) == 6);
        // Question 2:
        System.out.println((sumDiv(1184) == 1210) && (sumDiv(1210) == 1184));
        // Question 3:

        boolean foundNumbers = false;
        for (int i = 1; !foundNumbers && i <= 500; i++) {
            // On vérifie si on a trouvé les nombres au préalable avec `foundNumbers` afin d'éviter d'avoir le meme
            // couple 2 fois (sinon on a un couple (x; y) et un couple (y; x))
                for (int j = 1; !foundNumbers && j <= 500; j++) {
                    if (i != j && sumDiv(i) == j && sumDiv(j) == i) {
                        System.out.println("Les nombres " + i + " et " + j + " sont amicaux et inférieurs à 500.");
                        foundNumbers = true;
                    }
                }
        }

        // Question 4:
        // Ici, pour chaque i >= 10000 on realise le test des nombres Amicaux pour chaque 10000 <= i < j
        System.out.println("Recherche d'un couple de nombres amicaux >=10000. Cela peut prendre du temps...");
        boolean foundNumbers10000 = false;
        for (int i=10000; !foundNumbers10000; i++) {
            for (int j = 10000; !foundNumbers10000 && j < i; j++) {
                if (sumDiv(j) == i && sumDiv(i) == j) {
                    System.out.println("Les nombres " + j + " et " + i + " sont amicaux et supérieurs à 10000");
                    foundNumbers10000 = true;
                }
            }
        }
    }
}
