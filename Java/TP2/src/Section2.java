class Section2 {

    public static String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    // QUESTION 1 
    // Déclarez la fonction letterToInt ci-dessous
    public static int letterToInt(String s) {
        return alphabet.indexOf(s);
    }


    // QUESTION 2  
    // Déclarez la fonction cesarLetter ci-dessous
    public static String cesarLetter(String ref, String s) {
        if (!s.equals(" ")) {
            int decalage = letterToInt(ref) - letterToInt("A");
            return String.valueOf(alphabet.charAt((letterToInt(s) + decalage) % 26));
        }
        return " ";
    }


    // QUESTION 3
    // Déclarez la fonction cesar  ci-dessous
    public static String cesar(String ref, String s) {
        String result = "";
        for (int i=0; i<s.length(); i++) {
            result += cesarLetter(ref, String.valueOf(s.charAt(i)));
        }
        return result;
    }


    // QUESTION 4
    // Déclarez la fonction deCesarLetter ci-dessous
    public static String deCesarLetter(String ref, String s) {
        if (!s.equals(" ")) {
            int decalage = 26 - letterToInt(ref);
            return String.valueOf(alphabet.charAt((letterToInt(s) + decalage) % 26));
        }
        return " ";
    }


    // QUESTION 5
    // Déclarez la fonction deCesar ci-dessous
    public static String deCesar(String ref, String s) {
        String result = "";
        for (int i=0; i<s.length(); i++) {
            result += deCesarLetter(ref, String.valueOf(s.charAt(i)));
        }
        return result;
    }


    // QUESTION 6
    // Déchiffrez le message "DOHD MDFWD HVW" dans la fonction principale



    /* ********************************************************** */
    /* FONCTION PRINCIPALE                                        */
    /* ********************************************************** */


    public static void main(String[] args) {
        System.out.println(letterToInt("A") == 0);
        System.out.println(letterToInt("Z") == 25);
        System.out.println(letterToInt("*") == -1);

        System.out.println(cesarLetter("D", "B").equals("E"));
        System.out.println(cesarLetter("H", " ").equals(" "));
        System.out.println(cesarLetter("C", "Z").equals("B"));

        System.out.println(cesar("D", "BONJOUR").equals("ERQMRXU"));
        System.out.println(deCesarLetter("D","E").equals("B"));
        System.out.println(deCesarLetter("D","F").equals("C"));
        System.out.println(deCesarLetter("Z","B").equals("C"));


        System.out.println(deCesar("D", "ERQMRXU").equals("BONJOUR"));
        System.out.println(deCesar("D", "DOHD MDFWD HVW")); // ==> ALEA JACTA EST
    }


    /* ********************************************************** */
    /* FONCTIONS AUXILIAIRES                                      */
    /* ********************************************************** */
    // caractère à une position donnée
    public static String charAtPos(String s, int i) {
        return String.valueOf(s.charAt(i));
    }

    // test d'égalité entre chaînes de caractères
    public static boolean stringEquals(String s, String t) {
        return s.equals(t);
    }
}
