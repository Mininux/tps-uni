public class Recherche {
    public static int cherche(char c, String s) {
        return s.indexOf(c);
    }

    public static void main(String[] args) {
        System.out.println(cherche('a', "école"));
        System.out.println(cherche('a', "babar"));
    }
}
