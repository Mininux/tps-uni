public class Palindromes {
    // Question 1:
    public static String reverse(String text) {
        String reversed = "";
        for (int i = text.length()-1; i >= 0; i--) {
            reversed += text.charAt(i);
        }
        return reversed;
    }

    // Question 2:
    public static boolean palindrome(String text) {
        return reverse(text).equals(text);
    }

    // Question 3:
    public static boolean palindrome_bis(String text) {
        for (int i=0; i<text.length(); i++) {
            if (text.charAt(i) != text.charAt(text.length() -1 - i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        // test
        System.out.println(reverse("hello").equals("olleh"));
        System.out.println(palindrome_bis("rotor") == true);
        System.out.println(palindrome_bis("ressasser") == true);
        System.out.println(palindrome_bis("abricot") == false);
    }
}
