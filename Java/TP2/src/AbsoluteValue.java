public class AbsoluteValue {

    public static int myAbs(int x) {
        if (x >= 0) {
            return x;
        } else {
            return -x;
        }
    }

    public static int newAbs(int x) {
        if (x < 10) {
            return (int) (Math.sqrt(x * x));
        } else if (x >= 10 && x < 100) {
            return (int) (Math.sqrt(x * x));
        } else if (x == 100) {
            return -100;
        } else {
            return (int) (Math.pow(Math.sqrt(Math.sqrt(x * x)), 2));
        }
    }

    public static void main(String[] args) {
        // Question 1:
        System.out.println(myAbs(0));
        // Question 2:
        /*
        System.out.println("Entrée : " + 0);
        System.out.println("Sortie  myAbs : " + myAbs (10));
        System.out.println("Entrée : " + 0);
        System.out.println("Sortie  myAbs : " + myAbs (0));
        System.out.println("Entrée : " + 1);
        System.out.println("Sortie  myAbs : " + myAbs (1));
        */

        // Question 3:
        /*
        for (int i=-10; i<=10; i++) {
            System.out.println("Entrée : " + i);
            System.out.println("Sortie  myAbs : " + myAbs (i));
            }
        */

        // Question 4:
        /*
        for (int i=-10; i<=0; i++) {
            System.out.println("Entrée : " + i);
            int sortie = myAbs (i);
            System.out.print("Sortie  myAbs : " + sortie);
            System.out.println((sortie == -i) ? " (valide)" : " (erreur)");
        }

        for (int i=1; i<=10; i++) {
            System.out.println("Entrée : " + i);
            int sortie = myAbs (i);
            System.out.print("Sortie  myAbs : " + sortie);
            System.out.println((sortie == i) ? " (valide)" : " (erreur)");
        }
        */

        // Question 5
        /*
        for (int i=-100; i<=99; i++) {
            System.out.println("Entrée : " + i);
            int sortie = myAbs(i);
            System.out.print("Sortie myAbs : " + sortie);

            if (i<0 && sortie == -i || i>=0 && sortie == i) {
                System.out.println(" (valide)");
            } else {
                System.out.println(" (erreur)");
            }
        }
        */

        // Question 6:
        for (int i=-10; i<100; i++) {
            System.out.println((newAbs(i)>=0) ? "ok": "Erreur pour x= " + i);
        }

        // Question 7:
        for (int i=-200; i<=199; i++) {
            System.out.print(i + ": ");
            System.out.println((newAbs(i) == Math.abs(i)) ? "ok": "ko");
        }
        // ==> Les fonctions ne coïncident plus à partir de la valeur 100
    }
}
