import java.util.Arrays;

public class Voyelles {
    public static int vowels(String text) {
        text = text.toLowerCase(); // On rend le string entièrement en minuscule pour simplifier la suite
        int vowelCount=0;
        for (int i=0; i<text.length(); i++) {
            if ("aeiouy".indexOf(text.charAt(i)) != -1) { // Si le caractère à la position i est une voyelle...
                vowelCount++;
            }
        }
        return vowelCount;
    }
    public static void main(String[] args) {
        // Tests:
        System.out.println(vowels("Hello World!")==3);
        System.out.println(vowels("ZTPbTCxlmW")==0);
        System.out.println(vowels("AAAtfRxUIyyE")==8);
    }
}
