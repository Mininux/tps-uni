public class Concatenation {
    public static String concatNTimes(String s, int n) {
        if (n>=0) {
            return s.repeat(n);
        } else {
            return "";
        }
    }

    public static void main(String[] args) {
        // Tests:
        for (int i=-5; i<=5; i++) {
            String output = concatNTimes("a", i);
            String expected="";
            if (i<=0) {
                expected="";
            } else if (i>0) {
                expected = "a".repeat(i);
            }
            System.out.println(output.equals(expected));
        }
    }
}
