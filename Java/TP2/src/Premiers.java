public class Premiers {
    public static boolean isPrime(int n) {
        if (n != 1) {
            for (int i = 2; i <= Math.sqrt(n); i++) {
                if (n % i == 0) { // Si i divise n
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static int sumPrime(int n) {
        int total=0;
        for (int i=2; i<=n; i++) {
            if (isPrime(i)) {
                total+=i;
            }
        }
        return total;
    }

    public static void main(String[] args) {
        // Tests:
        int[] entrees = {1, 2, 3, 4, 5, 6, 7, 8, 10, 13, 15, 17, 21, 29, 100003};
        boolean[] sorties_attendues = {false, true, true, false, true, false, true, false, false, true, false, true, false, true, true};
        for (int i = 0; i < sorties_attendues.length; i++) {
            System.out.println(isPrime(entrees[i]) == sorties_attendues[i]);
        }

        System.out.println(sumPrime(10));
    }
}
