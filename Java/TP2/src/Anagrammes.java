public class Anagrammes {
    public static String suppression(char c, String s) {
        int delIndex = s.indexOf(c);
        if (delIndex != -1) { // Si c contient s
            return s.substring(0, delIndex) + s.substring(delIndex + 1);
        }
        return s;
    }

    public static boolean scrabble(String mot, String lettres_disponibles) {
        for (int i=0; i<mot.length(); i++) {
            String nv_lettres_disponibles = suppression(mot.charAt(i), lettres_disponibles);
            if (nv_lettres_disponibles.equals(lettres_disponibles)) {
                return false;
            } else {
                lettres_disponibles = nv_lettres_disponibles;
            }
        }
        return true;
    }

    public static boolean anagramme(String u, String v) {
        return scrabble(u, v);
    }

    public static void main(String[] args) {
        System.out.println(suppression('a', "baldaquin").equals("bldaquin"));
        System.out.println(suppression('d', "fleur").equals("fleur"));
        System.out.println(suppression('b', "banane").equals("anane"));
        System.out.println(suppression('e', "banane").equals("banan"));

        System.out.println(scrabble("maison", "auiysmzanpo")==true);
        System.out.println(scrabble("bungalows", "hbteslo")==false);

        System.out.println(scrabble("parisien", "aspirine")==true);
        System.out.println(scrabble("chaise", "disque")==false);
    }
}
