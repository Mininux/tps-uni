public class Conjugaison {
    static final String[] TERMINAISONS = {"e", "es", "e", "ons", "ez", "ent"};
    static final String[] PRONOMS = {"je ", "tu ", "il ", "nous ", "vous ", "ils "};
    //static final char[] VOYELLES = {'a', 'e', 'i', 'o', 'u', 'y'};
    static final String VOYELLES = "aeiouy";

    public static boolean isVowel(char lettre) {
        return VOYELLES.indexOf(lettre) != -1;
    }

    public static void conjugate(String verbe) {
        if (verbe.toLowerCase() != verbe) {
            System.out.print("Erreur: le verbe n'est pas en minuscule");
        } // On vérifie si le verbe est du 1er groupe :
        else if (verbe.substring(verbe.length() - 2).equals("er") && !verbe.equals("aller")) {
            String radical = verbe.substring(0, verbe.length() - 2); // On récupère le radicale en enlevant le "-er"
            for (int i = 0; i < PRONOMS.length; i++) {
                String pronom = PRONOMS[i];
                // Le cas où le radical commence par une voyelle, il faut remplacer "je" par "j'"
                if (pronom.equals("je ") && isVowel(verbe.charAt(0))) {
                    pronom = "j'";
                }
                String terminaison = TERMINAISONS[i];
                // Le cas où le radical se finit en g, il faut ajouter un e avant le "-ons" pour "nous"
                if (radical.charAt(radical.length()-1) == 'g' && terminaison.charAt(0) == 'o') {
                    terminaison = "e" + terminaison;
                }
                System.out.println(pronom + radical + terminaison);
            }
        }
    }

    public static void main(String[] args) {
        conjugate("aller");
        conjugate("parler");
        conjugate("abriter");
        conjugate("coder");
        conjugate("dormir");
        conjugate("manger");
    }
}
