public class Cadre {
    // Question 1:
    public static String line(int n) {
        return "#".repeat(n);
    }

    // Question 2:
    /*public static void frame(String text) {
        int frameWidth = text.length() + 2;
        System.out.println("+" + "-".repeat(frameWidth) + "+");
        System.out.println("| " + text + " |");
        System.out.println("+" + "-".repeat(frameWidth) + "+");
    }*/

    // Question 3:

    public static void frame(String s) {
        String[] lignes = s.split("\n");
        int maxWidth = 0;
        for (int i = 0; i < lignes.length; i++) {
            if (lignes[i].length() >= maxWidth) {
                maxWidth = lignes[i].length();
            }
        }
        String frameLine = "+" + "-".repeat(maxWidth + 2) + "+";

        System.out.println(frameLine);
        for (int i = 0; i < lignes.length; i++) {
            String currentLine = lignes[i];
            currentLine += " ".repeat(maxWidth - currentLine.length()); // On ajoute les espaces manquants pour les lignes trop courtes
            currentLine = "| " + currentLine + " |";
            System.out.println(currentLine);
        }
        System.out.println(frameLine);
    }


    public static void framel(String S) {
        int maxLen = 0;
        int lastNl = 0;
        S += "\n"; // On ajoute un dernier \n à la chaine de caractères, ça simplifie le comptage après
        for (int j = 0; j < S.length(); j++) {
            if ((S.charAt(j) == '\n' || j == S.length())) {
                maxLen = Math.max(maxLen, j - lastNl);
                lastNl = j+1;
            }
        }
    }


    public static void main(String[] args) {
        framel("55555\n999999999\nL\n333\n");
    }
}
