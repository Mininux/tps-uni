public class CharCode {

    public static int charToCode(char c) {
        return c;
    }

    public static char codeToChar(int code) {
        return (char) code;
    }

    public static void minuscule(char lettre) {
        if (charToCode('a') <= charToCode(lettre) && charToCode(lettre) <= charToCode('z')) {
            System.out.println("minuscule");
        } else if (charToCode('A') <= charToCode(lettre) && charToCode(lettre) <= charToCode('Z')) {
            System.out.println("MAJUSCULE");
        } else {
            System.out.println("5YMB0L3");
        }
    }

    public static void alphabet() {
        for (int i=charToCode('a'); i<=charToCode('z'); i++) {
            System.out.print(codeToChar(i));
        }
        System.out.println();
    }

    public static void main(String[] args) {
        // Question 1:
        System.out.println(charToCode('a'));
        System.out.println(charToCode('m'));
        System.out.println(charToCode('M'));

        // Question 2:
        minuscule('e');
        minuscule('U');
        minuscule('$');

        alphabet();
    }
}
