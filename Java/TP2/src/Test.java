import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        ex61(5);
    }

    public static void ex61(int k) {
        boolean found = false;
        for (int i=1; i<=k; i++) {
            if (readInt() == 42) {
                found = true;
            }
        }
        System.out.println((found)? "Gagné" : "Perdu");
    }

    public static int readInt() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
}
