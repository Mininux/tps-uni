class Section1 {

    /* ********************************************************** */
    /* EXERCICE 1 : ENTIERS NON SIGNES                            */
    /* ********************************************************** */


    // QUESTION 1 
    // Déclarez la fonction isBinaryEncoding ci-dessous

    public static boolean isBinaryEncoding(String s) {
        if (s.length() == 8 && s.replace("0", "").replace("1", "").isEmpty()) {
                return true;
            }
        return false;
    }

    // QUESTION 2  
    // Déclarez la fonction powerTwo ci-dessous

    public static int powerTwo(int n) {
        int total = 1;
        for (int i = 1; i <= n; i++) {
            total *= 2;
        }
        return total;
    }

    // QUESTION 3
    // Déclarez la fonction decode ci-dessous
    public static int decode(String s) {
        if (isBinaryEncoding(s)) {
            int total = 0;
            for (int i = 7; i >= 0; i--) {
                total += Character.getNumericValue(s.charAt(i)) * powerTwo(8 - i - 1);
            }
            return total;
        }
        return -1;
    }


    // QUESTION 4
    // Déclarez la *procédure* encodeAndPrint ci-dessous
    public static void encodeAndPrint(int x) {
        // On utilise l'algo d'Euclide étendu
        while (true) {
            int r = x % 2;
            System.out.print(r);
            x /= 2;
            if (x==0) break;;
        }
        System.out.println();
    }


    // QUESTION 5
    // Déclarez la fonction encode ci-dessous
    public static String encode(int x) {
        String binary = "";
        while (true) {
            int r = x % 2;
            binary = r + binary;
            x /= 2;
            if (x==0) break;
        }
        binary = "0".repeat(8 - binary.length()) + binary; // On ajoute les 0 avant le nombre afin d'obtenir un octet
        return binary;
    }


    // QUESTION 6
    // Ecrivez le test dans la fonction main




    /* ********************************************************** */
    /* EXERCICE 2 : INVERSE                              */
    /* ********************************************************** */


    // QUESTION 1 
    // Déclarez la fonction inverse ci-dessous
    public static String inverse(String s) {
        String inverted = "";
        for (int i=0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            if (currentChar == '0') {
                inverted += '1';
            } else if (currentChar == '1') {
                inverted += '0';
            } else {
                inverted += currentChar;
            }
        }
        return inverted;
    }


    // QUESTION 2  
    // Déclarez les fonctions encodeInv et decodeInv ci-dessous
    public static String encodeInv(int x) {
        return inverse(encode(x));
    }

    public static int decodeInv(String s) {
        return decode(inverse(s));
    }



    /* ********************************************************** */
    /* EXERCICE 3 : ENTIERS SIGNES                                */
    /* ********************************************************** */


    // QUESTION 1 
    // Déclarez la fonction isNegative ci-dessous
    public static boolean isNegative(String s) {
        return s.charAt(0) == '1';
    }


    // QUESTION 2  
    // Déclarez la fonction decodeNeg ci-dessous
    public static int decodeNeg(String s) {
        int abs = decode("0" + s.substring(1)); // On décode un octet formé des 7 derniers bits seulement
        if (s.charAt(0) == '0') {
            return abs;
        } else {
            return -abs;
        }
    }


    // QUESTION 3
    // Déclarez la fonction encodeNeg ci-dessous
    public static String encodeNeg(int x) {
        if (-127 <= x && x <= 127) {
            if (x < 0) {
                return "1" + encode(-x).substring(1);
            } else {
                return encode(x);
            }
        }
        return "";
    }


    // QUESTION 4
    // Déclarez la procédure testFinal ci-dessous
    public static void testFinal() {
        boolean correct = false;
        for (int i=0; i<=127; i++) {
            correct = encode(i).equals(encodeNeg(i));
        }
        System.out.println(correct ? "Tout va bien" : "Il y a un souci");
    }



    /* ********************************************************** */
    /* FONCTION PRINCIPALE                                        */
    /* ********************************************************** */


    public static void main(String[] args) {
        // Mes tests
        /*
        System.out.println(isBinaryEncoding("01010101") == true);
        System.out.println(isBinaryEncoding("0101010A") == false);
        System.out.println(isBinaryEncoding("010101010") == false);
        System.out.println(isBinaryEncoding("0101010101AB") == false);

        System.out.println(powerTwo(3) == 8);
        System.out.println(powerTwo(10) == 1024);

        System.out.println(decode("11111110") == 254);
        System.out.println(decode("10001000") == 136);

        encodeAndPrint(254);
        encodeAndPrint(136);

        System.out.println(encode(253).equals("11111101"));
        System.out.println(encode(15).equals("00001111"));

        System.out.println(inverse("010").equals("101"));
        System.out.println(inverse("bob1081").equals("bob0180"));

        System.out.println(encodeInv(253).equals("00000010"));
        System.out.println(encodeInv(15).equals("11110000"));
        System.out.println(decodeInv("11101101") == 18);

        System.out.println(isNegative("10000001") == true);
        System.out.println(isNegative(encode(10)) == false);

        System.out.println(decodeNeg("00000110") == 6);
        System.out.println(decodeNeg("11000001") == -65);

        System.out.println(encodeNeg(-17).equals("10010001"));
        System.out.println(encodeNeg(9).equals("00001001"));
        */
        // End mes tests

        // Ex 14 Question 6 :
        for (int i=0; i <= 255; i++) {
             // Cela devrait etre toujours vrai puisqu'un nombre est égal à lui-meme quelle que soit la base dans
             // laquelle on l'écrit
            //System.out.println(decode(encode(i))==i);
        }

        // Ex 16 Q4:
        testFinal();
    }



    /* ********************************************************** */
    /* FONCTIONS AUXILIAIRES                                      */
    /* ********************************************************** */


    // caractère à une position donnée
    public static String charAtPos(String s, int i) {
        return String.valueOf(s.charAt(i));
    }

    // test d'égalité entre chaînes de caractères
    public static boolean stringEquals(String s, String t) {
        return s.equals(t);
    }


}
