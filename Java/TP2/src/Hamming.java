public class Hamming {
    public static int hamming(String a, String b) {
        if (a.length() != b.length()) {
            return -1;
        } else {
            int distance = 0;
            for (int i = 0; i <= a.length() - 1; i++) {
                if (a.charAt(i) != b.charAt(i)) {
                    distance += 1;
                }
            }
            return distance;
        }
    }


    public static void main(String[] args) {
        System.out.println(hamming("aaba", "aaha") == 1);
        System.out.println(hamming("poire", "pomme") == 2);
        System.out.println(hamming("stylo", "bouteille") == -1);
    }
}
