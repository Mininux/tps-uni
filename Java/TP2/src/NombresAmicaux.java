public class NombresAmicaux {
    // Question 1:
    public static int sumDiv(int n) {
        int sum = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) { // Si i divise n
                sum += i;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        // Question 2:
        System.out.println((sumDiv(1184) == 1210) && (sumDiv(1210) == 1184));
        // Question 3:

        boolean foundNumbers = false;
        for (int i = 1; i <= 500; i++) {
            if (!foundNumbers) {
                for (int j = 1; j <= 500; j++) {
                    if (i != j && sumDiv(i) == j && sumDiv(j) == i) {
                        System.out.println("Les nombres " + i + " et " + j + " sont amicaux et inférieurs à 500.");
                        foundNumbers = true;
                    }
                }
            } else {
                break;
            }
        }

    }
}
