import java.io.Console;
import java.util.Random;

public class PierreFeuilleCiseauxArray {
    public static Random rand = new Random();
    public static Console console = System.console();

    public static String[] coups = {"Pierre", "Feuille", "Ciseaux"};

    public static String tirage() { // Choisi un coup au hasard entre Pierre Feuille et Ciseaux et le renvoie
        return coups[rand.nextInt(3)];
    }

    public static String coupJoueur() { // Fais choisir un coup au joueur et le renvoie
        if (console == null) {
            // Avec `System.console()` plutot que `Scanner`, il faut impérativement lancer le programme depuis un terminal pour que ça fonctionne
            System.out.println("Vous devez exécuter ce programme dans la console !");
            return "";
        }
        System.out.println("Pierre, Feuille ou Ciseaux ?\nVotre réponse : ");
        return console.readLine();
    }

    // Pour un coup donné, renvoie le coup qui le bat (ex: coupContre(Pierre)="Feuille")
    public static String coupContre(String coup) {
        String resultat = "";
        switch (coup) {
            case "Pierre":
                resultat = "Feuille";
                break;
            case "Feuille":
                resultat = "Ciseaux";
                break;
            case "Ciseaux":
                resultat = "Pierre";
                break;
        }
        return resultat;
    }

    // Fait jouer une manche entre l'ordinateur et le joueur et renvoie un char correspondant au résultat
    public static char uneManche() {
        String joueur = coupJoueur();
        String ordi = tirage();

        if (joueur.equals(coupContre(ordi))) {
            return 'J';
        } else if (joueur.equals(ordi)) {
            return 'E';
        }
        return 'O';
    }

    public static void chifoumi(int n) {  // Fais jouer n coups au joueur et à l'ordi
        String results = "";
        for (int i = 1; i <= n; i++) {
            System.out.println("Manche " + i + ":");
            char result = uneManche();
            switch (result) {
                case 'J':
                    System.out.println("Vous gagnez la manche !");
                    break;
                case 'O':
                    System.out.println("L'Ordinateur gagne la manche !");
                    break;
                case 'E':
                    System.out.println("Égalité !");
                    break;
            }
            results += result;
            System.out.println();
        }
        System.out.println("Résumé de la partie: " + results);
    }


    public static void main(String[] args) {
        //System.out.println(uneManche());
        chifoumi(5);
    }
}
