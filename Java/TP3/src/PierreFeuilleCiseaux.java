import java.io.Console;
import java.util.Random;
import java.util.Scanner;

public class PierreFeuilleCiseaux {
    public static Random rand = new Random();
    public static Console console = System.console();
    public static Scanner sc = new Scanner(System.in);


    public static String tirage() { // Choisi un coup au hasard entre Pierre Feuille et Ciseaux et le renvoie
        String result = "";
        switch (rand.nextInt(3)) {
            case 0:
                result = "Pierre";
                break;
            case 1:
                result = "Feuille";
                break;
            case 2:
                result = "Ciseaux";
                break;
        }
        System.out.println("L'ordinateur joue... " + result + " !");
        return result;
    }

    public static String coupJoueur() { // Fais choisir un coup au joueur et le renvoie
        System.out.println("Pierre, Feuille ou Ciseaux ?\nVotre réponse : ");
        if (console == null) { // On n'a pas lancé depuis la console, on essaie d'utiliser scanner plutôt
            return sc.nextLine();
        }
        return console.readLine();
    }

    // Pour un coup donné, renvoie le coup qui le bat (ex: coupContre(Pierre)="Feuille")
    public static String coupContre(String coup) {
        String resultat = "";
        switch (coup) {
            case "Pierre":
                resultat = "Feuille";
                break;
            case "Feuille":
                resultat = "Ciseaux";
                break;
            case "Ciseaux":
                resultat = "Pierre";
                break;
        }
        return resultat;
    }

    // Fait jouer une manche entre l'ordinateur et le joueur et renvoie un char correspondant au résultat
    public static char uneManche() {
        String joueur = coupJoueur();
        String ordi = tirage();

        if (joueur.equals(coupContre(ordi))) {
            return 'J';
        } else if (joueur.equals(ordi)) {
            return 'E';
        }
        return 'O';
    }

    public static void chifoumi(int n) {  // Fait jouer n coups au joueur et à l'ordi
        String results = "";
        int nbVictoiresJ = 0;
        int nbVictoiresO = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Manche " + i + ":");
            char result = uneManche();
            switch (result) {
                case 'J':
                    System.out.println("Vous gagnez la manche !");
                    nbVictoiresJ++;
                    break;
                case 'O':
                    System.out.println("L'Ordinateur gagne la manche !");
                    nbVictoiresO++;
                    break;
                case 'E':
                    System.out.println("Égalité !");
                    break;
            }
            results += result;
            System.out.println();
        }
        System.out.println();
        if (nbVictoiresJ > nbVictoiresO) {
            System.out.println("Vous gagnez la partie !");
        } else if (nbVictoiresO > nbVictoiresJ) {
            System.out.println("L'ordinateur gagne la partie !");
        } else {
            System.out.println("Égalité, personne n'a gagné !");
        }
        System.out.println("Résumé de la partie: " + results);
    }

    // Avec Lézard et Spock:
    public static String coupJoueurBonus() { // Fais choisir un coup au joueur et le renvoie
        System.out.println("Pierre, Feuille, Ciseaux, Lezard ou Spock ?\nVotre réponse : ");
        if (console == null) { // On n'a pas lancé depuis la console, on essaie d'utiliser scanner plutôt
            return sc.nextLine();
        }
        return console.readLine();
    }

    // Cette fois, on arrête au bout d'un certain nombre de victoires.
    public static void chifoumiBonus() {
        System.out.println("Nombre de Victoires requis ? ");
        int maxVictoires = sc.nextInt();
        sc.nextLine(); // Nécessaire sinon le prochain nextLine ne va pas fonctionner


        String results = "";
        int nbVictoiresJ = 0;
        int nbVictoiresO = 0;
        for (int i = 1; Math.max(nbVictoiresO, nbVictoiresJ) < maxVictoires; i++) {
            System.out.println("Manche " + i + ":");
            char result = uneMancheBonus();
            switch (result) {
                case 'J':
                    nbVictoiresJ += 1;
                    System.out.println("Vous gagnez la manche !");
                    break;
                case 'O':
                    nbVictoiresO += 1;
                    System.out.println("L'Ordinateur gagne la manche !");
                    break;
                case 'E':
                    System.out.println("Égalité !");
                    break;
            }
            results += result;
            System.out.println();
        }
        System.out.println("Résumé de la partie: " + results);
    }

    public static String tirageBonus() {
        String result = "";
        switch (rand.nextInt(5)) {
            case 0:
                result = "Pierre";
                break;
            case 1:
                result = "Feuille";
                break;
            case 2:
                result = "Ciseaux";
                break;
            case 3:
                result = "Spock";
                break;
            case 4:
                result = "Lezard";
                break;
        }
        System.out.println("L'ordinateur joue... " + result + " !");
        return result;
    }

    public static char uneMancheBonus() {
        String joueur = coupJoueurBonus();
        String ordi = tirageBonus();

        if (joueur.equals(ordi)) {
            return 'E';
        } else if (joueur.equals("Pierre") && (ordi.equals("Ciseaux") || ordi.equals("Lezard")) || joueur.equals("Feuille") && (ordi.equals("Pierre") || ordi.equals("Spock")) || joueur.equals("Ciseaux") && (ordi.equals("Feuille") || ordi.equals("Lezard")) || joueur.equals("Lezard") && (ordi.equals("Feuille") || ordi.equals("Spock")) || joueur.equals("Spock") && (ordi.equals("Pierre") || ordi.equals("Ciseaux"))) {
            return 'J';
        }
        return 'O';
    }


    // La dernière question de l'exercice "bonus" est dans le fichier PierreFeuilleCiseauxBonus.java,
    // en effet elle nécessitait trop de modifications et rendait le code confus.

    public static void main(String[] args) {
        //System.out.println(uneManche());
        //chifoumi(5);
        chifoumiBonus();
    }
}
