import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class PierreFeuilleCiseauxBonus {
    public static Random rand = new Random();
    public static Scanner sc = new Scanner(System.in);

    // Pour convetir les coups (String) en int et vice-versa.
    final private static String[] coupsArr = {"Pierre", "Feuille", "Ciseaux", "Lezard", "Spock"};

    // Pour chaque coup, on établit un tableau contenant les coups capables de le battre
    // Par exemple, la Pierre (index 0) est battue par la Feuille (1) et Spock (4),
    // la feuille (index 1) est battue par les Ciseaux (2) et le Lézard (3)
    private static final int[][] coupContre = {{1, 4}, {2, 3}, {0, 4}, {0, 2}, {1, 3}};

    // Contient les fréquences des coups les plus joués (initialement 0.2, car 0.2 * 5coups = 1)
    private static final double[] frequencesCoups = {0.2, 0.2, 0.2, 0.2, 0.2};

    public static void chifoumiBonus() {
        System.out.println("Nombre de Victoires requis ? ");
        int maxVictoires = sc.nextInt();
        sc.nextLine(); // Nécessaire sinon le prochain nextLine ne va pas fonctionner

        String results = "";
        int nbVictoiresJ = 0;
        int nbVictoiresO = 0;

        for (int i = 1; Math.max(nbVictoiresO, nbVictoiresJ) < maxVictoires; i++) {
            System.out.println("Manche " + i + ":");
            char result = uneMancheBonus();
            switch (result) {
                case 'J':
                    nbVictoiresJ += 1;
                    System.out.println("Vous gagnez la manche !");
                    break;
                case 'O':
                    nbVictoiresO += 1;
                    System.out.println("L'Ordinateur gagne la manche !");
                    break;
                case 'E':
                    System.out.println("Égalité !");
                    break;
            }
            results += result;
            System.out.println();
        }
        if (nbVictoiresJ > nbVictoiresO) {
            System.out.println("Vous gagnez la partie !");
        } else if (nbVictoiresO > nbVictoiresJ) {
            System.out.println("L'ordinateur gagne la partie !");
        }
        System.out.println("Résumé de la partie: " + results);
    }

    public static int tirageBonus() {
        return rand.nextInt(5); // On renvoie un nombre entre 0 et 4, correspondant à un coup
    }


    private static int coupToInt(String coup) {
        return Arrays.asList(coupsArr).indexOf(coup);
    }
    public static int coupJoueurBonus() { // Fait choisir un coup au joueur, le convertit en int et le renvoie.
        System.out.println("Pierre, Feuille, Ciseaux, Lezard ou Spock ?");
        return coupToInt(sc.nextLine());
    }

    // Renvoie un numéro indiquant si le coup du joueur bat celui de l'ordi.
    // 0 -> égalité, 1 -> joueur 1, 2 --> joueur 2
    public static char quiGagne(int joueur, int ordi) {
        if (joueur == ordi) {
            return 'E';
        } else if (coupContre[ordi][0] == joueur || coupContre[ordi][1] == joueur) {
            return 'J';
        } else {
            return 'O';
        }
    }

    public static char uneMancheBonus() {
        int joueur = coupJoueurBonus();
        // On passe le coup du joueur à l'ordi, non pas pour qu'il triche et joue le coup contre,
        // mais pour mettre à jour les fréquences des coups.
        int ordi = coupOrdi(joueur);
        return quiGagne(joueur, ordi);
    }


    // Méthode : on stocke dans le tableau `frequencesCoups` les fréquences des coups joués depuis
    // le début de la partie. On réduit légèrement ces fréquences à chaque manche, pour se mettre
    // à jour par rapport au comportement actuel du joueur. Si la différence entre la
    // fréquence du coup le plus joué et du moins joué est suffisamment grande, alors on essaie
    // si possible de jouer un coup qui bat les 2 coups les plus joués. Sinon, on utilise au hasard
    // un des deux coups qui bat le coup le plus joué.
    // Si la différence n'est pas significative, alors on joue au hasard.
    public static int coupOrdi(int dernierCoupJoueur) {
        double freqCoupMax1 = 0; // Fréquence du coup le plus joué
        double freqCoupMax2 = 0; // Fréquence du 2e coup le plus joué (utilisé pour décider lors d'un dilemme)
        double freqCoupMin = 1; // Fréquence du coup le moins joué
        int coupMax1 = 0; // Coup le plus joué
        int coupMax2 = -1; // 2e coup le plus joué

        for (int i = 0; i < frequencesCoups.length; i++) { // Quel est le coup le + joué et le - joué ?
            if (frequencesCoups[i] > freqCoupMax1) {
                coupMax1 = i;
                freqCoupMax1 = frequencesCoups[i];
            } else if (frequencesCoups[i] > freqCoupMax2) {
                coupMax2 = i;
                freqCoupMax2 = frequencesCoups[i];
            }

            if (frequencesCoups[i] < freqCoupMin) {
                freqCoupMin = frequencesCoups[i];
            }
        }


        // On met les fréquences à jour, *après* avoir déterminé
        // les coups les plus joués sinon c'est de la triche.
        for (int i = 0; i < frequencesCoups.length; i++) {

            if (dernierCoupJoueur == i) {
                frequencesCoups[i] += (frequencesCoups[i] <= 1) ? 0.1 : 0; // On limite les fréquences à 1 maximum
            } else {
                frequencesCoups[i] *= 0.8;
            }
        }

        int coup;
        if (freqCoupMax1 - freqCoupMin >= 0.2) { // Si la différence de fréquences est significative, jouer stratégiquement
            coup = coupContre2(coupMax1, coupMax2);
        } else { // Sinon jouer au hasard
            coup = tirageBonus();
        }
        //System.out.println(coup);
        System.out.println("L'ordinateur joue... " + coupsArr[coup] + " !");
        return coup;
    }

    // Renvoie le coup le plus favorable selon les 2 coups les plus joués du joueur
    public static int coupContre2(int coupMax1, int coupMax2) {
        int[] contreCoup1 = coupContre[coupMax1]; // Coups capables de battre le coup le + joué
        int[] contreCoup2 = coupContre[coupMax2]; // Coups capables de battre le 2e coup le + joué
        // Si on remarque une intersection entre les tableaux des coups les plus favorables, alors l'utiliser

        // Par exemple, si le coupMax1 correspond à Feuille et coupMax2 à Spock, on joue Pierre, car il
        // bat les deux.
        if (contreCoup1[0] == contreCoup2[0] || contreCoup1[0] == contreCoup2[1]) {
            return contreCoup1[0];
        } else if (contreCoup1[1] == contreCoup2[0] || contreCoup1[1] == contreCoup2[1]) {
            return contreCoup1[1];
        }
        // Sinon jouer au hasard parmi les 2 coups capables de battre le coup le + joué.
        return contreCoup1[rand.nextInt(2)];
    }


    public static void main(String[] args) {
        chifoumiBonus();
    }
}
