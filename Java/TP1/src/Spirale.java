public class Spirale {
    public static Turtle turtle;
    static final int PICTURE_WIDTH = 1024;
    static final int PICTURE_HEIGHT = 1024;

    static final int COTE_SPIRAL = 200; // Longueur du coté initial de la spirale
    static final int NOMBRE_BOUCLES = 10; // Nombre de boucles de la spirale
    static final int DIMINUTION_COTE_SPIRAL = 20; // De combien de pixels le coté devrait diminuer à chaque boucle

    public static void main(String[] args) {
        turtle = new Turtle(PICTURE_WIDTH, PICTURE_HEIGHT);
        turtle.setspeed(1);

        turtle.up();
        turtle.moveto(-400, 0);
        turtle.down();

        int cote = COTE_SPIRAL;
        for (int i = 0; i <= NOMBRE_BOUCLES; i++) {
            turtle.setheading(0);
            turtle.forward(cote);
            turtle.setheading(90);
            turtle.forward(cote);
            cote -= DIMINUTION_COTE_SPIRAL;
            // On vérifie que le cté n'est pas négatif, sinon la spirale devient monstrueuse...
            if (cote < 0) {
                cote = 0;
            }
            turtle.setheading(180);
            turtle.forward(cote);
            turtle.setheading(-90);
            turtle.forward(cote);
            cote -= DIMINUTION_COTE_SPIRAL;
            if (cote < 0) {
                cote = 0;
            }
        }
        turtle.exitonclick();
    }
}
