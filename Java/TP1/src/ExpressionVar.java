public class ExpressionVar {

    public static void main(String[] args) {
        int x=73*3; // --> Triple de 73
        int y=(2*x)/3; // --> 2/3 de x
        System.out.println((x+y)/5); // --> Affiche la division entière par 5 de la somme de x et de y (= 73)
    }
}
