public class Museum {
    /* EXERCICE 15 */
	/* Prend une année de naissance et l’année courante en arguments et qui
	renvoie un entier valant soit 0, soit 5 soit 3 correspondant à la réduction obtenue pour la personne née dans
	l’année précisée en argument */
    public static int reduction(int birthyear, int year) {
        int age = year - birthyear;
        if (age < 18) {
            return 5;
        } else if (age >= 60) {
            return 3;
        } else {
            return 0;
        }
    }

    /* EXERCICE 16*/
    public static int price(int birthyear, int year, int price) {
        int total = price - reduction(birthyear, year);
        if (total < 0) {
            return 0;
        } else {
            return total;
        }
    }

    /*************************************************************/
    /* PROGRAMME PRINCIPAL*/
    public static void main(String[] args) {


        System.out.println("====Exo 15====");
        testInt("reduction(2004,2021)", reduction(2004, 2021), 5);
        testInt("reduction(1958,2021)", reduction(1958, 2021), 3);
        testInt("reduction(2003,2021)", reduction(2003, 2021), 0);
        testInt("reduction(1940,2000)", reduction(1940, 2000), 3);
        testInt("reduction(1979,2000)", reduction(1979, 2000), 0);

        System.out.println("====Exo 16====");
        testInt("price(2005,2021,25)", price(2005, 2021, 25), 20);
        testInt("price(1958,2021,2)", price(1958, 2021, 2), 0);
        testInt("price(1992,2021,10)", price(1992, 2021, 10), 10);

    }

    /*************************************************************/
    /* NE PAS MODIFIER */

    /*************************************************************/
    public static void testInt(String cmd, int exp, int val) {
        System.out.println(cmd + " == " + exp);
        if (exp != val) {
            System.out.println("Ce n'est pas la bonne réponse!");
        }
    }
    /*************************************************************/


}
