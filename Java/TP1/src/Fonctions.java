public class Fonctions {
    // Question 1:
    // Retourne le résultat de la division entière de 10 par x: int
    static int div10(int x) {
        return 10 / x;
    }

    // Question 4
    // Renvoie la somme du produit de x et de y, du produit de x et de z et du produit de y et de z
    static int sumproduct(int x, int y, int z) {
        return (x * y) + (x * z) + (y * z);
    }

    public static void main(String[] args) {
        // Question 2:
        System.out.println(div10(10)); // --> Affiche 1;
        System.out.println(div10(2)); // --> Affiche 5;
        // System.out.println(div10(0)); // Exception: / by zero

        /***********************************************************/
        // Question 3:
        // Somme des évaluations de div10(x) pour x valant 1, 3, 5 et 7
        int somme = 0;
        for (int i = 1; i <= 7; i += 2) {
            somme += div10(i);
        }
        System.out.println(somme); // --> Affiche 16

        /***********************************************************/
        // Question 5:
        System.out.println(sumproduct(
                2,
                2 + 1,
                div10(2)
        )); // --> Affiche 31

        /***********************************************************/
        // Question 6:
        // div10 (); // --> Erreur
        /*
        java: method div10 in class Fonctions cannot be applied to given types;
            required: int
            found:    no arguments
            reason: actual and formal argument lists differ in length
         */

        // div10 (33, 37); // --> Erreur

        /*
        java: method div10 in class Fonctions cannot be applied to given types;
            required: int
            found:    int,int
            reason: actual and formal argument lists differ in length
         */

        // div10 ("Dark Vador"); // --> Erreur

        /*
        java: incompatible types: java.lang.String cannot be converted to int
         */
    }
}
