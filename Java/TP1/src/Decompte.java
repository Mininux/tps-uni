public class Decompte {

   static void rebours(int n) {
       for (int i=n; i>=0; i--) {
           System.out.println(i);
       }
   }

    public static void main(String[] args) {
        rebours(3); // --> Affiche 3 \n 2 \n 1 \n 0
        rebours(-10); // --> Ne fait rien
    }
}
