public class Conditions {
    // Question 2
    // Renvoie la valeur absolue de x: int
    static int absolute(int x) {
        if (x < 0) {
            return -x;
        } else {
            return x;
        }
    }

    // Question 3
    // Affiche "x+y=z", "x-y=z" ou "Rien du tout!" selon les valeurs x: int, y:int, z:int d'entrée
    static void solve(int x, int y, int z) {
        if (x+y == z) {
            System.out.println("x + y = z");
        } else if (x - y == z) {
            System.out.println("x - y = z");
        } else {
            System.out.println("Rien du tout!");
        }
    }

    public static void main(String[] args) {
        // Question 2
        System.out.println(absolute(37)); // --> affiche 37
        System.out.println(absolute(-37)); // --> affiche 37

        // Question 3
        solve(2, 3, 5); // --> affiche "x + y = z"
        solve(5, 3, 2); // --> affiche "x - y = z"
        solve(2, -3, 5); // --> affiche x - y = z"
        solve(10, 100, 1000); // --> affiche "Rien du tout!"
    }
}
