public class LapinBlanc {
    // Convertit s (en secondes) en heures, minutes, secondes
    static void deSecondes(int s) {
        int heures = s/(3600);
        s -= heures * (3600); // on récupère le reste
        int minutes = s/60;
        s -= minutes * 60; // on récupère le reste

        System.out.println(heures);
        System.out.println(minutes);
        System.out.println(s);
    }

    static void versSecondes(int h, int m, int s) {
        System.out.println(h*3600 + m*60 + s);
    }

    public static void main(String[] args) {
        deSecondes(3725);

        versSecondes(3, 2, 1);
    }
}
