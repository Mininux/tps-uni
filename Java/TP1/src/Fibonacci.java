public class Fibonacci {
    static int fibonacci(int n) {
        if (n <= 2) {
            return 2;
        } else {
            return fibonacci(n-2) + fibonacci(n-1);
        }
    }

    public static void main(String[] args) {
        /*
        Pour n>2:
        fibonacci(n+2)=(fibonacci(n)*2 + fibonacci(n+1) - fibonacci(n)
        fibonacci(n+2)= fibonacci(n) + fibonacci(n+1)

        Solution de l'éngime : 288 lapins, soit 144 paires
         */

        /****TESTS****/
        System.out.println(fibonacci(1)==2* 1);
        System.out.println(fibonacci(2)==2* 1);
        System.out.println(fibonacci(3)==2* 2);
        System.out.println(fibonacci(4)==2* 3);
        System.out.println(fibonacci(5)==2* 5);
        System.out.println(fibonacci(6)==2* 8);
        /****FIN TESTS****/

        System.out.println(fibonacci(12)); // --> Affiche 288
    }
}
