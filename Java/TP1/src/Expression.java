public class Expression {
    public static void main(String[] args) {
        System.out.println(21*2); // --> Affiche le Double de 21
        System.out.println(1025/32); // --> Affiche la Div entière par 32 de 1025
        System.out.println((1000+24)/128); // --> Affiche la Div entière par 128 de la somme de 1000 et 24
        System.out.println((771*80085)/3); // --> Affiche la Div entière par 3 du produit de 771 et de 80085
    }
}
