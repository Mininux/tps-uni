/*
  Un programme simple pour dessiner des creneaux
*/
public class Creneaux{
	static final int NOMBRE_CRENEAUX = 10; // Nombre de créneaux
	static final int HAUTEUR_CRENEAUX = 40; // Hauteur des créneaux
	static final int LARGEUR_CRENEAUX = 40; // Largeur des créneaux
	// De combien de pxiels la largeur devrait etre diminuée après chaque créneau
	static final int DIMINUTION_LARGEUR = 5;

    // Donnée de configuration du dessin
    public static Turtle turtle;
    static int picture_width  = 1024;
    static int picture_height = 1024;

    public static void main (String[] args) {
	// Configuration de la tortue
	turtle = new Turtle (picture_width,picture_height);
	turtle.setspeed (1000);

	// Ici commence le code pour dessiner
	turtle.up();
	turtle.moveto(-400,0);
	turtle.down();

	int largeur = HAUTEUR_CRENEAUX;
	for(int i=0;i<NOMBRE_CRENEAUX;i=i+1){
		// On vérifie si la largeur est pas trop faible pour etre visible (et surtout qu'elle reste positive)
		if (largeur > 10 + DIMINUTION_LARGEUR) {
			largeur -= DIMINUTION_LARGEUR;
		} else {
			largeur = 10;
		}
	    turtle.setheading (0);
	    turtle.forward (largeur);
	    turtle.setheading (90);
	    turtle.forward (HAUTEUR_CRENEAUX);
	    turtle.setheading (0);
	    turtle.forward (largeur);
	    turtle.setheading (-90);
	    turtle.forward (HAUTEUR_CRENEAUX);
	}

	//Code d'attente pour la fermeture de la fenetre
	turtle.exitonclick ();
    }

}
