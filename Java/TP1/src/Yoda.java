public class Yoda {
    // Question 2
    static void yoda(int n) {
        for (int i=0; i<n; i++) {
            System.out.println("May the force be with you.");
        }
    }

    public static void main(String[] args) {
        // Question 2
        yoda(100);
        /*********************************/
        // Question 3
        // Affiche 42*100=42000 fois "May the force be with you"
        for (int j = 0; j < 42; j++) {
            for (int i=0; i<1; i++) {
                System.out.println("May the force be with you.");
            }
        }
        // On appelle cela une boucle imbriquée car on a une boucle for dans une autre boucle for
    }
}
