public class BouclesDOr {

   static void count_to(int n) {
       for (int i=0; i<=n; i++){
           System.out.println(i);
       }
   }

   static void filter_even(int n) {
       if (n%2 == 0) {
           System.out.println(n);
       } else {
           System.out.println("...");
       }
   }

    public static void main(String[] args) {
        count_to(100); // --> Affiche les nombres de 0 à 100

        for (int i=0; i<=65535; i++) {
            filter_even(i);
        } // --> Affiche les nombres paires entre 0 et 65535
    }
}
