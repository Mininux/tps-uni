import java.io.File;
import java.util.Random;
import java.util.Scanner;

public class Labyrinthe {
    public static Random rand = new Random();

    //Cette fonction prend en argument le nom d'un fichier
    //contenant la description d'un labyrinthe
    //et renvoie la liste de liste d'entiers correspondante
    public static int[][] chargeLabyrinthe(String nomFichier) {
        int[][] labyrinthe = {};
        try {
            Scanner sc = new Scanner(new File(nomFichier)).useDelimiter("\n");
            int c = 0;
            //On compte le nombre de lignes
            while (sc.hasNext()) {
                c = c + 1;
                String tmp = sc.next();
            }
            labyrinthe = new int[c][];
            sc = new Scanner(new File(nomFichier)).useDelimiter("\n");
            int i = 0;
            while (sc.hasNext()) {
                String ligne = sc.next();
                String[] splitLigne = ligne.split(",");
                labyrinthe[i] = new int[splitLigne.length];
                for (int j = 0; j < splitLigne.length; j = j + 1) {
                    labyrinthe[i][j] = Integer.parseInt(splitLigne[j]);
                }
                i = i + 1;
            }

        } catch (Exception e) {
            System.out.println("Probleme dans la lecture du fichier " + nomFichier);
            e.printStackTrace();
        }
        return labyrinthe;
    }

    public static void afficheLab(int[][] l) {
        for (int[] row : l) {
            for (int c : row) {
                System.out.print((c == 0) ? "X" : " ");
            }
            System.out.println();
        }
    }

    // Affiche les numéros des colonnes et des lignes, et affiche des nombres plutôt que des X
    public static void debugLab(int[][] l) {
        System.out.println("_".repeat(l[0].length + 4));
        System.out.print("   ");
        for (int i = 0; i < l[0].length; i++) {
            System.out.print(i % 10);
        }
        System.out.println();
        for (int i = 0; i < l.length; i++) {
            System.out.print(i + " ".repeat((i == 0) ? 1 : ((int) Math.log10(l.length - 1) + 1) - (int) (Math.log10(i))) + "|");
            for (int c : l[i]) {
                System.out.print(c % 10);
            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println("_".repeat(l[0].length + 2));
    }

    // Renvoie une copie du labyrinthe passé en entrée
    public static int[][] copieLab(int[][] l) {
        int[][] result = new int[l.length][l[0].length];
        for (int i = 0; i < l.length; i++) {
            for (int j = 0; j < l[i].length; j++) {
                result[i][j] = l[i][j];
            }
        }
        return result;
    }

    // Renvoie la valeur de la case au dessus de la case à la position y x, -1 si elle n'existe pas
    public static int caseHaut(int[][] l, int y, int x) {
        if (y <= 0 || y > l.length || x < 0 || x >= l[0].length) return -1;
        else return l[y - 1][x];
    }

    // Renvoie la valeur de la case en dessous de la case à la position y x, -1 si elle n'existe pas
    public static int caseBas(int[][] l, int y, int x) {
        if (y <= -2 || y >= l.length - 1 || x < 0 || x >= l[0].length) return -1;
        else return l[y + 1][x];
    }

    // Renvoie la valeur de la case à gauche de la case à la position y x, -1 si elle n'existe pas
    public static int caseGauche(int[][] l, int y, int x) {
        if (x <= 0 || x > l[0].length || y < 0 || y >= l.length) return -1;
        else return l[y][x - 1];
    }

    // Renvoie la valeur de la case à droite de la case à la position y x, -1 si elle n'existe pas
    public static int caseDroite(int[][] l, int y, int x) {
        if (x <= -2 || x >= l[0].length - 1 || y < 0 || y >= l.length) return -1;
        else return l[y][x + 1];
    }

    // Renvoie un tableau contenant les coordonnées des cases voisines vides (contenant des 1)
    public static int[][] voisisnsLibres(int[][] l, int y, int x) {
        int[][] temp = new int[4][];
        int nbCases = 0;
        if (caseHaut(l, y, x) == 1) {
            temp[nbCases] = new int[]{y - 1, x};
            nbCases++;
        }
        if (caseBas(l, y, x) == 1) {
            temp[nbCases] = new int[]{y + 1, x};
            nbCases++;
        }
        if (caseGauche(l, y, x) == 1) {
            temp[nbCases] = new int[]{y, x - 1};
            nbCases++;
        }
        if (caseDroite(l, y, x) == 1) {
            temp[nbCases] = new int[]{y, x + 1};
            nbCases++;
        }
        int[][] result = new int[nbCases][2];
        for (int i = 0; i < nbCases; i++) {
            result[i] = temp[i];
        }

        return result;
    }

    // remplace toutes les cases voisines vides par i+1
    public static void changeVoisins(int[][] l, int y, int x, int i) {
        for (int[] c : voisisnsLibres(l, y, x)) {
            l[c[0]][c[1]] = i + 1;
        }
    }

    // Réalise une étape de l'algorithme de parcours
    public static void etapeParcours(int[][] l, int i) {
        for (int j = 0; j < l.length; j++) {
            for (int k = 0; k < l[0].length; k++) {
                if (l[j][k] == i) {
                    changeVoisins(l, j, k, i);
                }
            }
        }
    }

    // Renvoie true ssi le labyrinthe est fini, ou qu'on est bloqués
    public static boolean finParcours(int[][] l) {
        if (l[l.length - 1][l[0].length - 1] != 1) {
            return true;
        }
        for (int i = 0; i < l.length; i++) {
            for (int j = 0; j < l[0].length; j++) {
                if (l[i][j] > 1 && (
                        caseHaut(l, i, j) == 1 ||
                                caseBas(l, i, j) == 1 ||
                                caseGauche(l, i, j) == 1 ||
                                caseDroite(l, i, j) == 1)
                ) {
                    return false;
                }
            }
        }

        return true;
    }

    // Réalise l'algorithme de parcours et renvoie le nombre minimum de cases
    public static int parcours(int[][] l) {
        int[][] copy = copieLab(l);
        copy[0][0] = 2;
        int i = 2;
        while (!finParcours(copy)) {
            etapeParcours(copy, i);
            i++;
        }
        if (copy[copy.length - 1][copy[0].length - 1] > 1) {
            return i - 1;
        } else {
            return -1;
        }
    }

    // Génère un labyrinthe traversable en a cases (algorithme très lent car reposant sur de l'aléatoire)
    public static int[][] genereLab(int n, int m, int a) {
        int[][] result;
        do {
            result = new int[n][m];
            result[0][0] = 1;
            result[n - 1][m - 1] = 1;
            for (int i = 0; i < a - 2; i++) { // a - 2 car on a déjà la case en haut à gauche et en bas à droite
                int randX;
                int randY;
                do {
                    randX = rand.nextInt(m);
                    randY = rand.nextInt(n);
                } while (result[randY][randX] != 0); // Pour éviter de tirer 2 fois la même case
                result[randY][randX] = 1;
            }
        } while (parcours(result) != a);
        return result;
    }


    // Un meilleur algorithme pour créer un leuse" notre abyrinthe et qui ne créé pas forcément un chemin direct vers la sortie
    // Ici, on "creuse" un chemin jusqu'à la sortie plutôt que de tirer des cases au hasard, c'est bien plus rapide.
    // Idéalement, il faudrait que l'on puisse revenir en arrière dans le chemin pour avoir des chemins plus étroites et
    // plus de bifurcations, mais ça deviendrait encore plus complexe.
    public static int[][] genereLabBetterAlgo(int n, int m, int a) {
        int[][] result = new int[n][m]; // On initialise le labyrinthe
        result[0][0] = 1;
        result[n - 1][m - 1] = 1;
        int currentY = 0;
        int currentX = 0;
        do {
//            debugLab(result);
            if (caseHaut(result, currentY, currentX) != 0 &&
                    caseDroite(result, currentY, currentX) != 0 &&
                    caseGauche(result, currentY, currentX) != 0 &&
                    caseBas(result, currentY, currentX) != 0
                    || parcours(result) > a
            ) {
                // On est bloqués, on réintialise le labyrinthe.
                result = new int[n][m];
                result[0][0] = 1;
                result[n - 1][m - 1] = 1;
                currentY = 0;
                currentX = 0;
                continue;
            }
            // On creuse dans une direction aléatoire
            int randDirection = rand.nextInt(4);
            switch (randDirection) {
                case 0:
                    if (caseHaut(result, currentY, currentX) == 0) {
                        currentY = currentY - 1;
                        result[currentY][currentX] = 1;
                        break;
                }
                case 1:
                    if (caseDroite(result, currentY, currentX) == 0) {
                        currentX = currentX + 1;
                        result[currentY][currentX] = 1;
                        break;
                }
                case 2:
                    if (caseBas(result, currentY, currentX) == 0) {
                        currentY = currentY + 1;
                        result[currentY][currentX] = 1;
                        break;
                }
                case 3:
                    if (caseGauche(result, currentY, currentX) == 0) {
                        currentX = currentX - 1;
                        result[currentY][currentX] = 1;
                        break;
                }
            }
        } while (parcours(result) != a);
        return result;
    }


    public static void main(String[] args) {
        int[][] lab = chargeLabyrinthe("labyrinthe1.csv");
//        afficheLab(lab);
//        int[] t = {-2, -1, 0, 1, 28, 29, 30, 31, 32};
//        for (int y : t) {
//            for (int x : t) {
//                System.out.println("y, x = " + y + ", " + x + "  " + caseDroite(lab, y, x));
//            }
//        }
//        int[][] l2 = {{1, 1, 1, 1}, {0, 1, 0, 1}, {0, 1, 1, 1}};
//        System.out.println(parcours(lab));

//        debugLab(l2);
//        System.out.println(parcours(l2));

//        int[][] l3 = {{1, 1}, {0, 1}, {0, 1}};
//        System.out.println(parcours(l3));
        afficheLab(genereLab(5, 5, 9));
        System.out.println("_".repeat(40));
//        int[][] l3 = {{1, 0, 0, 0, 0}, {0, 0, 1, 0, 0}, {1, 1, 1, 0, 0}, {1, 0, 0, 0, 0}, {1, 1, 1, 1, 1}, {0, 0, 0, 0, 1}};
//        System.out.println(parcours(l3));
        afficheLab(genereLabBetterAlgo(5, 5, 9));

        System.out.println("_".repeat(40));
        afficheLab(genereLabBetterAlgo(30, 30, 59));
    }
}
